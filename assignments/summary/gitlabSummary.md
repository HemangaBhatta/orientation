## Git Summary

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

Git is easy to learn and has a tiny footprint with lightning fast performance. It outclasses SCM tools like Subversion, CVS, Perforce, and ClearCase with features like cheap local branching, convenient staging areas, and multiple workflows.

The major difference between Git and any other VCS (Subversion and friends included) is the way Git thinks about its data. Conceptually, most other systems store information as a list of file-based changes. These other systems (CVS, Subversion, Perforce, Bazaar, and so on) think of the information they store as a set of files and the changes made to each file over time 

![](https://git-scm.com/images/branching-illustration@2x.png)

## Git Terminologies
 * ### Repository
  Git repository is a directory that stores all the files, folders, and content needed for your project.  
 * ### Remote 
  It is a shared repository that all team members use to exchange their changes.
 * ### Master
  The primary branch of all repositories. All committed and accepted changes should be on the master branch.
 * ### Branch
  A version of the repository that diverges from the main working project or master branch.
 * ### Clone
  A clone is a copy of a repository or the action of copying a repository.
 * ### Fetch
  By performing a Git fetch, you are downloading and copying that branch’s files to your workstation.
 * ### Fork
  Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.  
 * ### Merge
  Taking the changes from one branch and adding them into another (traditionally master) branch.
 * ### Push
  Updates a remote branch with the commits made to the current branch. 
 * ### Pull
  If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request.

  ![git](https://www.earthdatascience.org/images/earth-analytics/git-version-control/git-fork-clone-flow.png)

  ## **TO INSTALL GIT**
 For **linux** open the **terminal** and type
```
sudo apt-get install git

```
### **GIT COMMANDS**
```
git init    "to start a new repository "
git diff    "shows the file difference which are not yet staged"
git reset [file]   "unstages the file"
git rm [file]   "deletes the file from your working directory"
git log     "used to list the version history for the current branch"
git branch  "lists all the local branches in the current repository"
```
